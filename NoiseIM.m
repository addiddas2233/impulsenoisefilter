function [IM,Maska]=NoiseIM(im,p)
[n,m,k]=size(im);
%1-3 ���
for i=1:3
    %1 ���������� ����-�����
    x = rand(n,m);
    b=im(:,:,i);
    b(x < p/2) = 0; % Minimum value
    b(x >= p/2 & x < p) = 255; % Maximum (saturated) value
    IM1(:,:,i)=uint8(b);
    %2 ���������� ������
    x = rand(n,m);
    b=im(:,:,i);
    b(x < p/2) = randi([0,255]); % Minimum value
    b(x >= p/2 & x < p) = randi([0,255]); % Maximum (saturated) value
    IM2(:,:,i)=uint8(b);
    %3 ���������� � ��������
    x = rand(n,m);
    b=im(:,:,i);
    b(x < p/2) = randi([0,50]); % Minimum value
    b(x >= p/2 & x < p) = randi([200,255]); % Maximum (saturated) value
    IM3(:,:,i)=uint8(b);
end
IM{1}=IM1;
IM{2}=IM2;
IM{3}=IM3;

%4 �������� � ������ ����� ����-�����
x = rand(n,m);
b1=im(:,:,1);
b2=im(:,:,2);
b3=im(:,:,3);
b1(x < p/2) = 255*randi([0,1]); % Minimum value
b2(x < p/2) = 255*randi([0,1]); % Minimum value
b3(x < p/2) = 255*randi([0,1]); % Minimum value
b1(x >= p/2 & x < p) = 255*randi([0,1]); % Maximum (saturated) value
b2(x >= p/2 & x < p) = 255*randi([0,1]); % Maximum (saturated) value
b3(x >= p/2 & x < p) = 255*randi([0,1]); % Maximum (saturated) value
IM4(:,:,1)=uint8(b1);
IM4(:,:,2)=uint8(b2);
IM4(:,:,3)=uint8(b3);

%5 �������� � ������ ����� ���������
x = rand(n,m);
b1=im(:,:,1);
b2=im(:,:,2);
b3=im(:,:,3);
b1(x < p/2) = randi([0,255]); % Minimum value
b2(x < p/2) = randi([0,255]); % Minimum value
b3(x < p/2) = randi([0,255]); % Minimum value
b1(x >= p/2 & x < p) = randi([0,255]); % Maximum (saturated) value
b2(x >= p/2 & x < p) = randi([0,255]); % Maximum (saturated) value
b3(x >= p/2 & x < p) = randi([0,255]); % Maximum (saturated) value
IM5(:,:,1)=uint8(b1);
IM5(:,:,2)=uint8(b2);
IM5(:,:,3)=uint8(b3);

%6 �������� � ������ ����� ���������
x = rand(n,m);
b1=im(:,:,1);
b2=im(:,:,2);
b3=im(:,:,3);
b1(x < p/2) = randi([0,50]); % Minimum value
b2(x < p/2) = randi([0,50]); % Minimum value
b3(x < p/2) = randi([0,50]); % Minimum value
b1(x >= p/2 & x < p) = randi([200,255]); % Maximum (saturated) value
b2(x >= p/2 & x < p) = randi([200,255]); % Maximum (saturated) value
b3(x >= p/2 & x < p) = randi([200,255]); % Maximum (saturated) value
IM6(:,:,1)=uint8(b1);
IM6(:,:,2)=uint8(b2);
IM6(:,:,3)=uint8(b3);

IM{4}=IM4;
IM{5}=IM5;
IM{6}=IM6;

%����� �����
Maska1=GetErrorMask(im, IM1);

Maska2=GetErrorMask(im, IM2);

Maska3=GetErrorMask(im, IM3);

Maska4=GetErrorMask(im, IM4);

Maska5=GetErrorMask(im, IM5);

Maska6=GetErrorMask(im, IM6);


Maska{1}=Maska1;
Maska{2}=Maska2;
Maska{3}=Maska3;
Maska{4}=Maska4;
Maska{5}=Maska5;
Maska{6}=Maska6;
end