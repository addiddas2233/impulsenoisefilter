function VMF(imageForNoiseRemoval, noiseMaska, aHalfWindowSize, resultFileName)
imwrite(imageForNoiseRemoval, strcat('before', resultFileName));
[ver, hor, color] = size(imageForNoiseRemoval);
wsize = (aHalfWindowSize*2+1)^2;
'run VMF'
% imwrite(imageForNoiseRemoval, resultFileName);
for i = aHalfWindowSize+1 : (ver - aHalfWindowSize)
    for j = aHalfWindowSize+1 : (hor - aHalfWindowSize)
        if noiseMaska(i, j) == 1
            for color = 1:3
                %window = imageForNoiseRemoval( :,:, color);
                window = imageForNoiseRemoval((i-aHalfWindowSize) : (i + aHalfWindowSize), (j-aHalfWindowSize) : (j + aHalfWindowSize), color);
                vec = int16(reshape(window, 1, wsize));
                result = zeros(1, wsize, 'int16');
                %                 vec
                for k = 1 : (wsize)
                    for kk = 1 : (wsize)
                        if k ~= kk
                            a = vec(k);
                            a = abs(a - vec(kk));
                            a = a + result(k);
                            a = mod(a, 256);
                            result(k) = a;%mod(result(k) + abs(vec(kk) - vec(k)), 256);
                        end
                    end
                    %  result(k) = mod(result(k), 255);
                end
                %                 result
                result = sort(result);
                imageForNoiseRemoval(i,j, color) = uint8(result(1));
            end
        end
    end
end
'end VMF'
imwrite(imageForNoiseRemoval, resultFileName);

end