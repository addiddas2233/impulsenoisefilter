function maska=GetErrorMask(originalImage, noiseImage)
[n,m,k]=size(originalImage);
maska=zeros(n,m);
for i=1:n
    for j=1:m
        if(originalImage(i,j,1)~=noiseImage(i,j,1) || originalImage(i,j,2)~=noiseImage(i,j,2) || originalImage(i,j,3)~=noiseImage(i,j,3))
            maska(i,j)=1;
        end
    end
end
end