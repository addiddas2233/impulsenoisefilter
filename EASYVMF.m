function EASYVMF(imageForNoiseRemoval, noiseMaska, aHalfWindowSize, resultFileName)
imwrite(imageForNoiseRemoval, strcat('before\', resultFileName));
[ver, hor, color] = size(imageForNoiseRemoval);
wsize = (aHalfWindowSize*2+1)^2;
'run EASYVMF'
% imwrite(imageForNoiseRemoval, resultFileName);
for i = aHalfWindowSize+1 : (ver - aHalfWindowSize)
    for j = aHalfWindowSize+1 : (hor - aHalfWindowSize)
        if noiseMaska(i, j) == 1
            for color = 1:3
                %window = imageForNoiseRemoval( :,:, color);
                window = imageForNoiseRemoval((i-aHalfWindowSize) : (i + aHalfWindowSize), (j-aHalfWindowSize) : (j + aHalfWindowSize), color);
                vec = reshape(window, 1, wsize);
                result = sort(vec);
                imageForNoiseRemoval(i,j, color) = result(uint8(wsize / 2));
            end
        end
    end
end
'end EASYVMF'
imwrite(imageForNoiseRemoval, resultFileName);
% imshow(imageForNoiseRemoval);
end